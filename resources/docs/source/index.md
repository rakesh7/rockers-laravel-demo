---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Admin User management


APIs for managing admin users
<!-- START_1c5034e53bb42c7e8872e0749e880ff1 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/admins" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/admins"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "test rakesh",
            "email": "rockersinfo483@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 3,
            "name": "patel rakeshss",
            "email": "rockersinfo@gmail.com",
            "created_at": "23-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/admins?page=1",
        "last": "http:\/\/localhost\/api\/admins?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/api\/admins",
        "per_page": 10,
        "to": 2,
        "total": 2
    }
}
```

### HTTP Request
`GET api/admins`


<!-- END_1c5034e53bb42c7e8872e0749e880ff1 -->

<!-- START_5d4f8024543e5e5cd4c65294b566be08 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/admins" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/admins"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/admins`


<!-- END_5d4f8024543e5e5cd4c65294b566be08 -->

<!-- START_37f538aa83d67494cdbacbe2e7dedf6b -->
## Show the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/admins/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/admins/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "error": "Call to a member function format() on null",
    "code": 500
}
```

### HTTP Request
`GET api/admins/{admin}`


<!-- END_37f538aa83d67494cdbacbe2e7dedf6b -->

<!-- START_44c71e5796225e711c412961d6211b0f -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/admins/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/admins/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/admins/{admin}`

`PATCH api/admins/{admin}`


<!-- END_44c71e5796225e711c412961d6211b0f -->

<!-- START_31c5826230dbb67675efd76b6e55b2a5 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/admins/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/admins/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/admins/{admin}`


<!-- END_31c5826230dbb67675efd76b6e55b2a5 -->

<!-- START_2e72cea0d721eeaa61b042b48fe01e5e -->
## Login api

> Example request:

```bash
curl -X POST \
    "http://localhost/api/adminlogin" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/adminlogin"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/adminlogin`


<!-- END_2e72cea0d721eeaa61b042b48fe01e5e -->

#Category management


APIs for managing categories
<!-- START_109013899e0bc43247b0f00b67f889cf -->
## Display a listing of the categories.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "omnis",
            "description": "Ut pariatur architecto ex. Illum dolore minima ipsa beatae ab omnis repellendus.",
            "status": "active",
            "image": "2.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 2,
            "name": "et",
            "description": "Et quisquam aliquam laboriosam illum aperiam id molestiae.",
            "status": "active",
            "image": "2.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 3,
            "name": "temporibus",
            "description": "Eum in eum fugit blanditiis.",
            "status": "active",
            "image": "3.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 4,
            "name": "velit",
            "description": "Harum iusto maxime sint placeat recusandae. Laudantium error numquam quidem aspernatur eaque ratione ab.",
            "status": "active",
            "image": "1.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 5,
            "name": "provident",
            "description": "Pariatur impedit fuga nemo ratione et. Quasi fugiat qui hic velit excepturi.",
            "status": "active",
            "image": "3.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 6,
            "name": "aliquam",
            "description": "Doloribus sed debitis nesciunt iusto consectetur ab.",
            "status": "active",
            "image": "2.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 7,
            "name": "non",
            "description": "Consequuntur deserunt autem aliquid. Accusantium odit omnis natus quod et quae sit.",
            "status": "inactive",
            "image": "1.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 8,
            "name": "aut",
            "description": "Tempore unde perspiciatis placeat consequatur eligendi sunt est.",
            "status": "inactive",
            "image": "1.jpg",
            "created_at": "17-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/categories?page=1",
        "last": "http:\/\/localhost\/api\/categories?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/api\/categories",
        "per_page": 10,
        "to": 8,
        "total": 8
    }
}
```

### HTTP Request
`GET api/categories`


<!-- END_109013899e0bc43247b0f00b67f889cf -->

<!-- START_2335abbed7f782ea7d7dd6df9c738d74 -->
## Store a newly created category in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/categories`


<!-- END_2335abbed7f782ea7d7dd6df9c738d74 -->

<!-- START_34925c1e31e7ecc53f8f52c8b1e91d44 -->
## Show the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/categories/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/categories/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "omnis",
        "description": "Ut pariatur architecto ex. Illum dolore minima ipsa beatae ab omnis repellendus.",
        "status": "active",
        "image": "2.jpg",
        "created_at": "17-01-2020"
    }
}
```

### HTTP Request
`GET api/categories/{category}`


<!-- END_34925c1e31e7ecc53f8f52c8b1e91d44 -->

<!-- START_549109b98c9f25ebff47fb4dc23423b6 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/categories/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/categories/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/categories/{category}`

`PATCH api/categories/{category}`


<!-- END_549109b98c9f25ebff47fb4dc23423b6 -->

<!-- START_7513823f87b59040507bd5ab26f9ceb5 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/categories/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/categories/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/categories/{category}`


<!-- END_7513823f87b59040507bd5ab26f9ceb5 -->

<!-- START_edb5f044def0afc43b544cf9168a4a6c -->
## Display a listing of the categories.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "omnis",
            "description": "Ut pariatur architecto ex. Illum dolore minima ipsa beatae ab omnis repellendus.",
            "status": "active",
            "image": "2.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 2,
            "name": "et",
            "description": "Et quisquam aliquam laboriosam illum aperiam id molestiae.",
            "status": "active",
            "image": "2.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 3,
            "name": "temporibus",
            "description": "Eum in eum fugit blanditiis.",
            "status": "active",
            "image": "3.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 4,
            "name": "velit",
            "description": "Harum iusto maxime sint placeat recusandae. Laudantium error numquam quidem aspernatur eaque ratione ab.",
            "status": "active",
            "image": "1.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 5,
            "name": "provident",
            "description": "Pariatur impedit fuga nemo ratione et. Quasi fugiat qui hic velit excepturi.",
            "status": "active",
            "image": "3.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 6,
            "name": "aliquam",
            "description": "Doloribus sed debitis nesciunt iusto consectetur ab.",
            "status": "active",
            "image": "2.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 7,
            "name": "non",
            "description": "Consequuntur deserunt autem aliquid. Accusantium odit omnis natus quod et quae sit.",
            "status": "inactive",
            "image": "1.jpg",
            "created_at": "17-01-2020"
        },
        {
            "id": 8,
            "name": "aut",
            "description": "Tempore unde perspiciatis placeat consequatur eligendi sunt est.",
            "status": "inactive",
            "image": "1.jpg",
            "created_at": "17-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/category?page=1",
        "last": "http:\/\/localhost\/category?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/category",
        "per_page": 10,
        "to": 8,
        "total": 8
    }
}
```

### HTTP Request
`GET category`


<!-- END_edb5f044def0afc43b544cf9168a4a6c -->

#Email Template management


APIs for managing email templates
<!-- START_40e931a22efcca1a2743b86a3b31d917 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/email_templates" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": null,
            "body": "Quam ullam non rerum. Dolorem quia repudiandae aut nihil libero illum optio.",
            "status": "inactive",
            "task": "error-adipisci-deleniti-et-est",
            "created_at": "20-01-2020"
        },
        {
            "id": 2,
            "name": null,
            "body": "Est voluptatum quia sequi vero sint doloremque. Excepturi voluptate perferendis optio perferendis neque.",
            "status": "inactive",
            "task": "dolorum-nemo-quo-repellendus-quis-similique-ratione-sit-culpa",
            "created_at": "20-01-2020"
        },
        {
            "id": 3,
            "name": null,
            "body": "Veritatis quasi ut architecto ut.",
            "status": "active",
            "task": "harum-eum-totam-ea",
            "created_at": "20-01-2020"
        },
        {
            "id": 4,
            "name": null,
            "body": "Accusamus doloremque veniam fugiat laborum. Nihil eos fugiat soluta voluptatem.",
            "status": "active",
            "task": "vero-earum-tempora-consequatur-aliquid-aut",
            "created_at": "20-01-2020"
        },
        {
            "id": 5,
            "name": null,
            "body": "Consequatur non reiciendis quia velit repellendus qui. Qui sed qui fugit qui fuga voluptas ut et.",
            "status": "active",
            "task": "quas-aut-voluptatem-suscipit-rerum-doloribus-fuga-perferendis-quibusdam",
            "created_at": "20-01-2020"
        },
        {
            "id": 6,
            "name": null,
            "body": "Quo et ut natus nulla rerum cum.",
            "status": "active",
            "task": "in-nam-vel-id-sit-consequuntur",
            "created_at": "20-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/email_templates?page=1",
        "last": "http:\/\/localhost\/api\/email_templates?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/api\/email_templates",
        "per_page": 10,
        "to": 6,
        "total": 6
    }
}
```

### HTTP Request
`GET api/email_templates`


<!-- END_40e931a22efcca1a2743b86a3b31d917 -->

<!-- START_89821e2e590f3ebc366285b9cdb0148a -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/email_templates/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "error": "View [create] not found.",
    "code": 500
}
```

### HTTP Request
`GET api/email_templates/create`


<!-- END_89821e2e590f3ebc366285b9cdb0148a -->

<!-- START_d71c67371f256a812ba8fd1eff03c76e -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/email_templates" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/email_templates`


<!-- END_d71c67371f256a812ba8fd1eff03c76e -->

<!-- START_1f749d8a48b203725d18a84fb06ce4ab -->
## Show the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/email_templates/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "error": "View [show] not found.",
    "code": 500
}
```

### HTTP Request
`GET api/email_templates/{email_template}`


<!-- END_1f749d8a48b203725d18a84fb06ce4ab -->

<!-- START_ef15ce91be248ed54d03d591ffab642e -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/email_templates/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "error": "View [edit] not found.",
    "code": 500
}
```

### HTTP Request
`GET api/email_templates/{email_template}/edit`


<!-- END_ef15ce91be248ed54d03d591ffab642e -->

<!-- START_5725060f444ea342298b86a56e5349a8 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/email_templates/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/email_templates/{email_template}`

`PATCH api/email_templates/{email_template}`


<!-- END_5725060f444ea342298b86a56e5349a8 -->

<!-- START_7d94b0cfedc29d349067a0d5bc3c14f6 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/email_templates/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email_templates/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/email_templates/{email_template}`


<!-- END_7d94b0cfedc29d349067a0d5bc3c14f6 -->

<!-- START_3934537b7185bf64cd987b8332dfbb91 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/emailtemplate" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/emailtemplate"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": null,
            "body": "Quam ullam non rerum. Dolorem quia repudiandae aut nihil libero illum optio.",
            "status": "inactive",
            "task": "error-adipisci-deleniti-et-est",
            "created_at": "20-01-2020"
        },
        {
            "id": 2,
            "name": null,
            "body": "Est voluptatum quia sequi vero sint doloremque. Excepturi voluptate perferendis optio perferendis neque.",
            "status": "inactive",
            "task": "dolorum-nemo-quo-repellendus-quis-similique-ratione-sit-culpa",
            "created_at": "20-01-2020"
        },
        {
            "id": 3,
            "name": null,
            "body": "Veritatis quasi ut architecto ut.",
            "status": "active",
            "task": "harum-eum-totam-ea",
            "created_at": "20-01-2020"
        },
        {
            "id": 4,
            "name": null,
            "body": "Accusamus doloremque veniam fugiat laborum. Nihil eos fugiat soluta voluptatem.",
            "status": "active",
            "task": "vero-earum-tempora-consequatur-aliquid-aut",
            "created_at": "20-01-2020"
        },
        {
            "id": 5,
            "name": null,
            "body": "Consequatur non reiciendis quia velit repellendus qui. Qui sed qui fugit qui fuga voluptas ut et.",
            "status": "active",
            "task": "quas-aut-voluptatem-suscipit-rerum-doloribus-fuga-perferendis-quibusdam",
            "created_at": "20-01-2020"
        },
        {
            "id": 6,
            "name": null,
            "body": "Quo et ut natus nulla rerum cum.",
            "status": "active",
            "task": "in-nam-vel-id-sit-consequuntur",
            "created_at": "20-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/emailtemplate?page=1",
        "last": "http:\/\/localhost\/emailtemplate?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/emailtemplate",
        "per_page": 10,
        "to": 6,
        "total": 6
    }
}
```

### HTTP Request
`GET emailtemplate`


<!-- END_3934537b7185bf64cd987b8332dfbb91 -->

#User management


APIs for managing user registration
<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Register api

> Example request:

```bash
curl -X POST \
    "http://localhost/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/register`


<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Login api

> Example request:

```bash
curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/login`


<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 19,
            "name": "test rakesh",
            "email": "rockersinfo@gmail.com",
            "created_at": "20-01-2020"
        },
        {
            "id": 23,
            "name": "test rakesh",
            "email": "rockersinfo1@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 25,
            "name": "test rakesh",
            "email": "rockersinfo21@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 27,
            "name": "test rakesh",
            "email": "rockersinfo221@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 28,
            "name": "test rakesh",
            "email": "rockersinfo4221@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 29,
            "name": "test rakesh",
            "email": "rockersinfo42121@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 31,
            "name": "test rakesh",
            "email": "rockersinfo48@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 32,
            "name": "test rakesh",
            "email": "rockersinfo483@gmail.com",
            "created_at": "23-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/users?page=1",
        "last": "http:\/\/localhost\/api\/users?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/api\/users",
        "per_page": 10,
        "to": 8,
        "total": 8
    }
}
```

### HTTP Request
`GET api/users`


<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->

<!-- START_12e37982cc5398c7100e59625ebb5514 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/users`


<!-- END_12e37982cc5398c7100e59625ebb5514 -->

<!-- START_8653614346cb0e3d444d164579a0a0a2 -->
## Show the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
{
    "error": "Does not exists any user with the specified identificator",
    "code": 404
}
```

### HTTP Request
`GET api/users/{user}`


<!-- END_8653614346cb0e3d444d164579a0a0a2 -->

<!-- START_48a3115be98493a3c866eb0e23347262 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/users/{user}`

`PATCH api/users/{user}`


<!-- END_48a3115be98493a3c866eb0e23347262 -->

<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/users/{user}`


<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->

<!-- START_3bcedda78ae45ef5c0f4c97a4963b7a1 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 19,
            "name": "test rakesh",
            "email": "rockersinfo@gmail.com",
            "created_at": "20-01-2020"
        },
        {
            "id": 23,
            "name": "test rakesh",
            "email": "rockersinfo1@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 25,
            "name": "test rakesh",
            "email": "rockersinfo21@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 27,
            "name": "test rakesh",
            "email": "rockersinfo221@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 28,
            "name": "test rakesh",
            "email": "rockersinfo4221@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 29,
            "name": "test rakesh",
            "email": "rockersinfo42121@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 31,
            "name": "test rakesh",
            "email": "rockersinfo48@gmail.com",
            "created_at": "23-01-2020"
        },
        {
            "id": 32,
            "name": "test rakesh",
            "email": "rockersinfo483@gmail.com",
            "created_at": "23-01-2020"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/user?page=1",
        "last": "http:\/\/localhost\/user?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/user",
        "per_page": 10,
        "to": 8,
        "total": 8
    }
}
```

### HTTP Request
`GET user`


<!-- END_3bcedda78ae45ef5c0f4c97a4963b7a1 -->


