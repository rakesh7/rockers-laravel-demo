<?php

namespace Modules\Category\Tests\Feature\Http\Controllers;


use Tests\TestCase;
use Modules\Category\Entities\Category;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Category\Repositories\Eloquent\EloquentCategoryRepository;
use Faker\Factory;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase;
    protected $faker;
    private $get_all_endpoint='api/categories/';
    private $get_search_endpoint='api/categories?search=';


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_all_category()
    {
        $this->withOutExceptionHandling();
        $response =$this->get($this->get_all_endpoint);

        $response->assertStatus(200);
        $this->assertArrayHasKey('data', $response);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_search_category()
    {
        $this->withOutExceptionHandling();
        $category= $this->create_category();
        $response =$this->get($this->get_search_endpoint. $category->name);

        $response->assertStatus(200);
        $this->assertArrayHasKey('data', $response);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_single_category()
    {

        $this->withOutExceptionHandling();
        $category= $this->create_category();


        $response =$this->get($this->get_all_endpoint.$category->id);

        $response->assertStatus(200);
        $this->assertArrayHasKey('data', $response);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_create_category() {
        $this->faker = Factory::create();
        $data = [
            'name' => $this->faker->name,
            'description' => $this->faker->paragraph,
        ];
        $response=$this->post(route('categories.store'), $data)
            ->assertStatus(200);
        $this->assertDatabaseHas('categories',  $data);
        $this->assertArrayHasKey('data',$response);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_create_category_validate() {
        $this->faker = Factory::create();
        $data = [
            'name' =>"",
            'description' => $this->faker->paragraph,
        ];
        $response=$this->post(route('categories.store'), $data)
            ->assertStatus(422);
        $this->assertArrayHasKey('error',$response);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_update_category() {
        $this->faker = Factory::create();
         $category= $this->create_category();
        $data = [
            'name' => $this->faker->name,
            'description' => $this->faker->paragraph,
        ];
        $response=$this->put($this->get_all_endpoint.$category->id, $data)
            ->assertStatus(200);
        $this->assertDatabaseHas('categories',  $data);
        $this->assertArrayHasKey('data',$response);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_delete_category() {
        $this->faker = Factory::create();
        $category= $this->create_category();
        $response=$this->delete($this->get_all_endpoint.$category->id)
            ->assertStatus(200);
        $this->assertArrayHasKey('data',$response);
    }
     /**
     * A basic unit test example.
     *
     * @return void
     */
    public function create_category()
    {
        return factory(Category::class)->create();

    }
}
