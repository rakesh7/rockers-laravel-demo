<?php

namespace Modules\Category\Tests;
use Faker\Factory;

use Orchestra\Testbench\Traits\CreatesApplication;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;
    protected $faker;
    public function setUp() : void {
        parent::setUp();
        $this->faker = Factory::create();
    }
}
