<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Application;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        DB::table('application_category')->truncate();
        $categoriesQuantity=2;
        factory(Category::class, $categoriesQuantity)->create()->each(
            function ($category) {
              $randomInt = random_int(1,2);
              $applications = Application::all()->random($randomInt)->pluck('id');

              $category->applications()->attach($applications);
            });
    }
}
