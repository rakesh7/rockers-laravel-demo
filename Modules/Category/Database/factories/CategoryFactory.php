<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Category\Entities\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(1),
        'status' => $faker->randomElement([Category::ACTIVE,Category::INACTIVE]),
        'image' =>  $faker->randomElement(['1.jpg', '2.jpg', '3.jpg']),
    ];
});
