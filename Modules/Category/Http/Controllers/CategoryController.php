<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Category\Entities\Category;
use Modules\Core\Http\Controllers\CoreController;
use Modules\Category\Transformers\CategoryTransformer;
use Modules\Category\Http\Requests\CreateCategoryRequest;
use Modules\Category\Http\Requests\UpdateCategoryRequest;
use Modules\Category\Repositories\CategoryRepositoryInterface;
/**
 * @group Category management
 *
 * APIs for managing categories
 */
class CategoryController extends CoreController
{
    private $category;

    public function __construct(CategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }
    /**
     * Display a listing of the categories.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        return CategoryTransformer::collection($this->category->serverPaginationFilteringFor($request));
    }



    /**
     * Store a newly created category in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        //
        $local_category=$this->category->create($request->all());
        return $this->successResponse( new CategoryTransformer($local_category),'Category is created');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Category $category)
    {
        return new CategoryTransformer($category);
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Category $category, UpdateCategoryRequest $request)
    {
        //
        $update_category=$this->category->update($category, $request->all());

        return $this->successResponse( new CategoryTransformer($update_category),'Category is updated');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Category $category)
    {
        //
        $this->category->destroy($category);
        return $this->successResponse( new CategoryTransformer($category),'Category is Deleted');
    }
}
