<?php

namespace Modules\Category\Entities;

use Modules\Core\Entities\Core;
use Vinkla\Hashids\Facades\Hashids;
use Modules\Core\Entities\Application;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Core
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable= ["name","description"];
    public function applications()
    {
    	return $this->belongsToMany(Application::class);
    }

}
