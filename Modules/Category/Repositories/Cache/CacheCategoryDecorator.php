<?php

namespace Modules\Category\Repositories\Cache;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Category\Entities\Category;
use Modules\Category\Repositories\CategoryRepositoryInterface;

class CacheCategoryDecorator extends BaseCacheDecorator implements CategoryRepositoryInterface
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(CategoryRepository $Category)
    {
        parent::__construct();
        $this->entityName = 'pages';
        $this->repository = $Category;
    }



    /**
     * Count all records
     * @return int
     */
    public function countAll()
    {
        return $this->remember(function () {
            return $this->repository->countAll();
        });
    }


}
