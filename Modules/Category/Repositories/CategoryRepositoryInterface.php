<?php

namespace Modules\Category\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Modules\Core\Repositories\BaseRepositoryInterface;
use Modules\Category\Entities\Category;

interface CategoryRepositoryInterface extends BaseRepositoryInterface
{


    /**
     * Count all records
     * @return int
     */
    public function countAll();

    public function serverPaginationFilteringFor(Request $request) : LengthAwarePaginator;
}
