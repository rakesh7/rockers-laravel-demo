<?php

namespace Modules\Category\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Modules\Category\Entities\Category;
use Illuminate\Database\Eloquent\Factory;
use Modules\Category\Repositories\CategoryRepositoryInterface;
use Modules\Category\Repositories\Cache\CacheCategoryDecorator;
use Modules\Category\Repositories\Eloquent\EloquentCategoryRepository;

class CategoryServiceProvider extends ServiceProvider
{
    private  $path ="Category";
    private $db_migrate_path= 'Database/Migrations';
    private $config_path= 'Config/config.php';
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->path, $this->db_migrate_path));

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->bind(CategoryRepositoryInterface::class, function () {
            $repository = new EloquentCategoryRepository(new Category());

            if (! Config::get('app.cache')) {
                return $repository;
            }

            return new CacheCategoryDecorator($repository);
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->path, $this->config_path) => config_path('category.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->path, $this->config_path), strtolower($this->path)
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/category');

        $sourcePath = module_path($this->path, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/category';
        }, \Config::get('view.paths')), [$sourcePath]), strtolower($this->path));
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/category');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, strtolower($this->path));
        } else {
            $this->loadTranslationsFrom(module_path($this->path, 'Resources/lang'), strtolower($this->path));
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->path, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
