<?php

namespace Modules\Category\Transformers;

use Modules\Core\Traits\HashIdTrait;
use Illuminate\Http\Resources\Json\Resource;

class CategoryTransformer extends Resource
{  use HashIdTrait;
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status,
            'image' => $this->image,
            'created_at' => $this->created_at->format('d-m-Y')
        ];
    }
}
