<?php
namespace Modules\Core\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

trait ApiResponser{

/**
     * Create a response object from the given data.
     *
     * @param  $data
     * @param  $message
     * @return $code
     */
	protected function successResponse($data ,$message,$code=200)
	{
		return  response()->json([
            'error' => false,
            'message' =>$message,
            'data' => $data,
        ],$code);
    }
    /**
     * Create a response object from the given error data.
     *
    * @param  $message
     * @return $code
     */
	protected function errorResponse($message ,$code)
	{
		return response()->json(['error'=>$message,'code'=>$code],$code);
	}




}
?>
