<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;

class Application extends Model
{
    protected $fillable = [];
    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }
}
