<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;

class Core extends Model
{
    const  ACTIVE ="active";
    const INACTIVE = "inactive";
}
