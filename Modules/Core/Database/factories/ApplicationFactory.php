<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Core\Entities\Application;

$factory->define(Application::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
    ];
});
