<?php

namespace Modules\User\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Core\Traits\EmailConfiguration;
use Modules\User\Emails\SendUserRegisterEmail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,EmailConfiguration;
      /**
     * @var configuration
     */
    public $configuration;
    /**
     * @var User
     */
    protected $user;

    /**
     * @var Mailable
     */
    protected $mail;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Mailable $mail
     */
    public function __construct(User $user, Mailable $mail)
    {
        $this->user = $user;
        $this->mail = $mail;
        $this->configuration = $this->email_configure();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Config::set( $this->configuration);
        //$mailer = app()->makeWith('user.mailer', $this->configuration);
        Mail::to($this->user->email)->send($this->mail);

    }
}
