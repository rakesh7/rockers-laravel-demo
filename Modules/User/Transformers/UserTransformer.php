<?php

namespace Modules\User\Transformers;

use Modules\Core\Traits\HashIdTrait;
use Illuminate\Http\Resources\Json\Resource;

class UserTransformer extends Resource
{  use HashIdTrait;
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created_at' => $this->created_at->format('d-m-Y')
        ];
    }
}
