<?php

namespace Modules\User\Tests\Feature\Emails;

use Tests\TestCase;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Bus;
use Modules\User\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Mail;
use Modules\User\Emails\BuildEmailTemplate;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\EmailTemplate\Entities\EmailTemplate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\EmailTemplate\Repositories\Eloquent\EloquentEmailTemplateRepository;

class BuildEmailTemplateTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->withOutExceptionHandling();
        Bus::fake();

        // Perform order shipping...



        $repository = new EloquentEmailTemplateRepository(new EmailTemplate());
        // Perform order shipping...
        $template='error-adipisci-deleniti-et-est';
        $mailable=new BuildEmailTemplate('error-adipisci-deleniti-et-est' ,$repository );
        $user = factory(User::class)->create();
        Bus::assertNotDispatched(SendEmailJob::class, function ($job,$e_mailable) use ($user,$mailable) {
            return $job->order->id === $user->id;
        });
    }
}
