<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'RegisterController@register');
Route::post('login', 'LoginController@login');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('users', 'UserController');
Route::resource('admins', 'AdminController');
Route::post('adminlogin', 'AdminController@login');
Route::get('email-test', function(){

	$details['email'] = 'rockersinfo@gmail.com';

    dispatch(new Modules\User\Jobs\SendEmailJob($details));

    dd('done');
});
