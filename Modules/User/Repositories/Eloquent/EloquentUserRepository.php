<?php

namespace Modules\User\Repositories\Eloquent;

use Illuminate\Http\Request;
use Modules\User\Jobs\SendEmailJob;
use Illuminate\Database\Eloquent\Builder;
use Modules\User\Emails\BuildEmailTemplate;
use Modules\EmailTemplate\Entities\EmailTemplate;
use Mcamara\LaravelLocalization\LaravelLocalization;

use Modules\User\Notifications\NotifyUserOnRegister;
use Modules\User\Repositories\UserRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\EmailTemplate\Repositories\Eloquent\EloquentEmailTemplateRepository;

class EloquentUserRepository extends EloquentBaseRepository implements UserRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function paginate($perPage = 15)
    {
        return $this->model->paginate($perPage);
    }



    /**
     * Count all records
     * @return int
     */
    public function countAll()
    {
        return $this->model->count();
    }

    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data)
    {

        $data['password'] = bcrypt($data['password']);
        $user = $this->model->create($data);
        $repository = new EloquentEmailTemplateRepository(new EmailTemplate());
        $mailable=new BuildEmailTemplate('error-adipisci-deleniti-et-est' ,$repository );
        dispatch(new SendEmailJob($user,$mailable));
        $user->notify(new NotifyUserOnRegister());

        return $user;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
       // event($event = new PageIsUpdating($model, $data));
       // $model->update($event->getAttributes());
       $model->update($data);
        return $model;
    }

    public function destroy($user)
    {
        //event(new PageWasDeleted($page));
        return $user->delete();
    }
    /**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $users = $this->allWithBuilder();

        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $users->where('name', 'LIKE', "%{$term}%");
            $users->orWhere('description', 'LIKE', "%{$term}%");;
        }

        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';
            $users->orderBy($request->get('order_by'), $order);
        }

        return $users->paginate($request->get('per_page', 10));
    }
}
