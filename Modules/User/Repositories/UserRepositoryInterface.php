<?php

namespace Modules\User\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Modules\Core\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{


    /**
     * Count all records
     * @return int
     */
    public function countAll();

    public function serverPaginationFilteringFor(Request $request) : LengthAwarePaginator;
}
