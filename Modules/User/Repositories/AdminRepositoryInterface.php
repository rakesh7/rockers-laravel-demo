<?php

namespace Modules\User\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Modules\Core\Repositories\BaseRepositoryInterface;

interface AdminRepositoryInterface extends BaseRepositoryInterface
{


    /**
     * Count all records
     * @return int
     */
    public function countAll();

    public function serverPaginationFilteringFor(Request $request) : LengthAwarePaginator;
}
