<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EmailTemplate\Repositories\EmailTemplateRepositoryInterface;

class BuildEmailTemplate extends Mailable
{
    use Queueable, SerializesModels;
/**
     * @var configuration
     */
    public $template;
    /**
     * @var User
     */
    protected $template_repository;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template,EmailTemplateRepositoryInterface $template_repository)
    {
        //
        $this->template=$template;
        $this->template_repository=$template_repository;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data=  $this->template_repository->findByTemplate($this->template);
        $data_array['body']=$this->from(['address' => 'patel.rakesh7@gmail.com', 'name' => 'App Name'])
        ->subject($data->subject)
        ->view('user::emails.test')->with(['data' => $data]);
        return   $data_array;
    }
}
