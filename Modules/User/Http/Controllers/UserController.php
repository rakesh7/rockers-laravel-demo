<?php
namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Entities\User;
use Modules\User\Transformers\UserTransformer;
use Modules\Core\Http\Controllers\CoreController;
use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Repositories\UserRepositoryInterface;
use Modules\Category\Http\Requests\UpdateCategoryRequest;
/**
 * @group User management
 *
 * APIs for managing users
 */
class UserController extends CoreController
{
    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $user= $this->user->serverPaginationFilteringFor($request);
        return UserTransformer::collection($user);
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        //
        $user=$this->user->create($request->all());
        return $this->successResponse( new UserTransformer($user),'Category is created');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(User $user)
    {
        return new UserTransformer($user);
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(User $user, UpdateCategoryRequest $request)
    {
        //
        $update_user=$this->user->update($user, $request->all());

        return $this->successResponse( new UserTransformer($update_user),'User is updated');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(User $user)
    {
        //
        $delete_user=$this->category->destroy($user);
        return $this->successResponse( new UserTransformer($user),'User is Deleted');
    }
}
