<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Entities\Admin;
use Modules\User\Transformers\AdminTransformer;
use Modules\Core\Http\Controllers\CoreController;
use Modules\User\Http\Requests\CreateAdminRequest;
use Modules\User\Repositories\AdminRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
/**
 * @group Admin User management
 *
 * APIs for managing admin users
 */
class AdminController extends CoreController
{
    private $admin;
    use AuthenticatesUsers;
    public function __construct(AdminRepositoryInterface $admin)
    {
        $this->middleware('guest:admin')->except('logout');
        $this->admin = $admin;
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $admin= $this->admin->serverPaginationFilteringFor($request);
        return AdminTransformer::collection($admin);
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CreateAdminRequest $request)
    {
        //
        $admin=$this->admin->create($request->all());
        return $this->successResponse( new AdminTransformer($admin),'Admin is created');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Admin $user)
    {
        return new AdminTransformer($user);
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Admin $admin, UpdateAdminRequest $request)
    {
        //
        $update_admin=$this->admin->update($admin, $request->all());
        return $this->successResponse( new AdminTransformer($update_admin),'admin is updated');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Admin $admin)
    {
        //
        $delete_admin=$this->admin->destroy($admin);
        return $this->successResponse( new AdminTransformer($admin),'Admin is Deleted');
    }
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::guard('admin')->user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['name'] =  $user->name;

            return $this->successResponse($success, 'User login successfully.');
        }
        else{
            return $this->errorResponse('Unauthorised.', 501);
        }
    }
}
