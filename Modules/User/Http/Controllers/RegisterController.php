<?php

namespace Modules\User\Http\Controllers;

use Validator;
use Notification;
use Illuminate\Http\Request;
use Modules\User\Entities\User;
use Nexmo\Laravel\Facade\Nexmo;

use Modules\User\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Auth;
use Modules\User\Transformers\UserTransformer;
use Modules\Core\Http\Controllers\CoreController;
use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Notifications\NotifyUserOnRegister;
use Modules\User\Repositories\UserRepositoryInterface;
/**
 * @group User management
 *
 * APIs for managing user registration
 */
class RegisterController extends CoreController
{
    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(CreateUserRequest $request)
    {

        $local_user=$this->user->create($request->all());
        return $this->successResponse( new UserTransformer($local_user),'User register successfully');
        /*$input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;*/
    }


}
