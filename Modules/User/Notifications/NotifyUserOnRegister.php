<?php

namespace Modules\User\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

use Nexmo\Notifications\Sms;
use Nexmo\Notifications\WhatsApp;
use Nexmo\Notifications\Facebook;
use Nexmo\Notifications\ViberServiceMessage;

use Nexmo\Notifications\Message\Text;

class NotifyUserOnRegister extends Notification
{
    use Queueable;

    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmoWhatsApp($notifiable)
    {
        return (new Text)->content('Merry Christmas WhatsApp!');
    }

    public function toNexmoFacebook($notifiable)
    {
        return (new Text)->content('Merry Christmas Facebook!');
    }

    public function toNexmoViberServiceMessage($notifiable)
    {
        return (new Text)->content('Merry Christmas Viber!');
    }

    public function toNexmo($notifiable)
    {
        return (new Text)->content('Merry Christmas SMS!');
    }
}
