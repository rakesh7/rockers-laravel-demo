<?php

namespace Modules\EmailTemplate\Entities;

use Modules\Core\Entities\Core;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Core
{
    protected $fillable = ['task','subject','type','body','status','application_id','from_address','reply_address','language_id'];
}
