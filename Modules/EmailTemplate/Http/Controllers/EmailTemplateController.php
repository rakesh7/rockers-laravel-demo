<?php

namespace Modules\EmailTemplate\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\EmailTemplate\Transformers\EmailTemplateTransformer;
use Modules\EmailTemplate\Repositories\EmailTemplateRepositoryInterface;
/**
 * @group Email Template management
 *
 * APIs for managing email templates
 */
class EmailTemplateController extends Controller
{
    private $email_template;

    public function __construct(EmailTemplateRepositoryInterface $email_template)
    {
        $this->email_template = $email_template;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        //$categories= CategoryTransformer::collection($this->category->paginate(2));
        ///return $this->successResponse( $categories);
        return EmailTemplateTransformer::collection($this->email_template->serverPaginationFilteringFor($request));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('emailtemplate::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('emailtemplate::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('emailtemplate::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
