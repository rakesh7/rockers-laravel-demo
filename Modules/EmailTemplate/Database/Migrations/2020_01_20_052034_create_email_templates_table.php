<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\EmailTemplate\Entities\EmailTemplate;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task');
            $table->string('subject');
            $table->string('from_address');
            $table->string('reply_address');
            $table->tinyInteger('language_id')->default(1);
            $table->tinyInteger('type')->default(1);
            $table->string('body',1000)->nullable();
            $table->string('status')->default(EmailTemplate::ACTIVE);
            $table->integer('application_id')->unsigned();
            $table->timestamps();
            $table->foreign('application_id')->references('id')->on('applications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_templates');
    }
}
