<?php

namespace Modules\EmailTemplate\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\EmailTemplate\Entities\EmailTemplate;

class EmailTemplateDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $appQuantity=5;
        factory(EmailTemplate::class,$appQuantity)->create();
        // $this->call("OthersTableSeeder");
    }
}
