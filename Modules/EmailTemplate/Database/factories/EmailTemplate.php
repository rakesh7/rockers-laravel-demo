<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Core\Entities\Application;
use Modules\EmailTemplate\Entities\EmailTemplate;

$factory->define(EmailTemplate::class, function (Faker $faker) {
    return [
        'from_address' => $faker->email,
        'reply_address' => $faker->email,
        'language_id' =>1,
        'subject' => $faker->word,
        'task' =>$faker->slug,
        'type' =>1,
        'body' => $faker->paragraph(1),
        'status' => $faker->randomElement([EmailTemplate::ACTIVE,EmailTemplate::INACTIVE]),
        'application_id' => Application::inRandomOrder()->first()->id,
    ];
});
