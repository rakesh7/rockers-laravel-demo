<?php

namespace Modules\EmailTemplate\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Modules\Core\Repositories\BaseRepositoryInterface;
use Modules\EmailTemplate\Entities\EmailTemplate;

interface EmailTemplateRepositoryInterface extends BaseRepositoryInterface
{


    /**
     * Count all records
     * @return int
     */
    public function countAll();
    public function findByTemplate($tempalte);
    public function serverPaginationFilteringFor(Request $request) : LengthAwarePaginator;
}
