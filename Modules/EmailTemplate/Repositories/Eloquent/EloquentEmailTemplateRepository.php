<?php

namespace Modules\EmailTemplate\Repositories\Eloquent;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\EmailTemplate\Entities\EmailTemplate;
use Modules\EmailTemplate\Repositories\EmailTemplateRepositoryInterface;

class EloquentEmailTemplateRepository extends EloquentBaseRepository implements EmailTemplateRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function paginate($perPage = 15)
    {
        return $this->model->paginate($perPage);
    }



    /**
     * Count all records
     * @return int
     */
    public function countAll()
    {
        return $this->model->count();
    }
    /**
     * Count all records
     * @return int
     */
    public function findByTemplate($template)
    {
        $email_template = $this->allWithBuilder();
       $data=  $email_template->where('task', 'LIKE', "%{$template}%")->first();


       return $data;
    }
    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data)
    {
       // event($event = new PageIsCreating($data));
       // $category = $this->model->create($event->getAttributes());
       $category = $this->model->create($data);
        return $category;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
       // event($event = new PageIsUpdating($model, $data));
       // $model->update($event->getAttributes());
       $model->update($data);
        return $model;
    }

    public function destroy($category)
    {
        //event(new PageWasDeleted($page));
        return $category->delete();
    }
    /**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $pages = $this->allWithBuilder();

        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $pages->where('name', 'LIKE', "%{$term}%");
            $pages->orWhere('description', 'LIKE', "%{$term}%");;
        }

        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';
            $pages->orderBy($request->get('order_by'), $order);
        }

        return $pages->paginate($request->get('per_page', 10));
    }
}
