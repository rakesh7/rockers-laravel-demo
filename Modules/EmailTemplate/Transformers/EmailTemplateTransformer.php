<?php

namespace Modules\EmailTemplate\Transformers;

use Modules\Core\Traits\HashIdTrait;
use Illuminate\Http\Resources\Json\Resource;

class EmailTemplateTransformer extends Resource
{  use HashIdTrait;
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' => $this->name,
            'body' => $this->body,
            'status' => $this->status,
            'task' => $this->task,
            'created_at' => $this->created_at->format('d-m-Y')
        ];
    }
}
